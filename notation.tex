% !TEX root = paper.tex
\section{SYNOPSIS: A notation for the description of Highly Configurable Services}

\emph{SYNOPSIS} (SimplY a NOtation to sPecify Service configuratIonS) is a text-based, human-readable notation to describe the decision space of services. It supports the specification of the configuration capabilities described in Sec.~\ref{sec:hcs}, common to the services of big providers such as Amazon, Rackspace or Microsoft, while remaining provider-agnostic. Figure~\ref{code:StorageSynopsis} shows the SYNOPSIS description of a simple block storage service, which we will use as running example to explain the notation.

\begin{figure}[t!!]
\caption{Simple Block Storage Service in SYNOPSIS.}
\label{code:StorageSynopsis}
%\begin{lstlisting}[style=synopsis,caption={Simple Block Storage Service in SYNOPSIS.},label={code:StorageSynopsis}]
\begin{lstlisting}[style=synopsis]
Service SimpleBlockStorage{

	%Terms
	## Selectable terms
	SSD: boolean;
	Size: int [1,1000];
	Region: {"USA", "EU", "JP"};
	## Derived terms
	costGBMonth: real [0.00,0.15]; ## euros/GB per month
	volumeCostMonth: real [0.00,150]; ## euros per month

	%Dependencies
	## pricing
	TABLE
	  Region       SSD     -> costGBMonth;
	    "USA"     true     ->    0.1;
	    "EU"      true     ->    0.12;
	    "JP"      true     ->    0.15;
	    "USA"     false    ->    0.05;
	    "EU"      false    ->    0.06;	    
	    "JP"      false    ->    0.08;	    
	ENDTABLE

	volumeCostMonth == costGBMonth * Size;	
}
\end{lstlisting}
\end{figure}

\begin{figure}[t!!]
\caption{Volume Storage HCS in SYNOPSIS.}
\label{code:StorageHCSSynopsis}
%\begin{lstlisting}[style=synopsis,caption={Volume Storage HCS in SYNOPSIS.},label={code:StorageHCSSynopsis}]
\begin{lstlisting}[style=synopsis]
Highly-configurable Service VolumeStorage{

	%Services
	SimpleBlockStorage[1,*]  storage;
	
	%Terms
	totalCostMonth: real [0.00,10000.00]; 
	discount: real [0.00,1000.00];

	%Dependencies
	## cost aggregation
	totalCostMonth == SUM(storage.volumeCostMonth);
	## discount policy
	totalCostMonth > 3000 -> discount == totalCostMonth*0.1;
}
\end{lstlisting}
\end{figure}


\subsection{Configurable and Highly Configurable Services}

SYNOPSIS notation let define the decision space of configurable services and \hcss -- the latter is an aggregation of the former. A SYNOPSIS document starts with the definition of the service type -- \texttt{Service} keyword for configurable services, and \texttt{Highly-configurable Service} for \hcss. While Figure~\ref{code:StorageSynopsis} shows a configurable storage service, Figure~\ref{code:StorageHCSSynopsis} shows an aggregation of block storages, giving rise to a storage \hcss.

A configurable service in SYNOPSIS has two sections, to declare terms and dependencies. In the terms section we declare the different terms -- both selectable and derived -- and their values, while in the dependencies section we describe the relationships among the different terms.

A \hcs in SYNOPSIS has three sections to declare the component services and their cardinality, global terms and global dependencies. For the two latter sections, the syntax is the same than for configurable services, and is describe in the following subsections. In the \texttt{\%Services} section we declare the service that compose the \hcs, their cardinality -- lower and upper bounds for the items -- and an alias to be referred.

\subsection{Decision Terms}

Decision terms are declared in SYNOPSIS in the terms section, denoted by the tag \texttt{\%Terms}.
As shown in Section~\ref{sec:hcs}, terms can be classified into two disjoint categories: selectable terms and derived terms. In this sense, both term types are described in this section and treated in the same way. The only requirements is that each term should have at least two different term values. 

The declaration syntax in shown in Figure~\ref{code:StorageSynopsis}. First, we define the term's name, and after a colon we declare its domain. Such domain can be enumerated, boolean, integer or real. In the case it is enumerated, the values are surrounded by quotation marks, separated by commas and enclosed by brackets. In the case of integer or real values, we have to use the keyword \texttt{int} or \texttt{real} and define the domain by means of the upper and lower bounds. For real domains, we can specify the float using as positions as necessary in the declaration.

In Figure~\ref{code:StorageSynopsis}, we can see the four different types of decision terms. We have also used comments to separate selectable terms and derived terms. For example, \texttt{SSD} is a boolean selectable term that indicates if the storage is ssd-based, \texttt{Size} is an integer selectable term that indicates the storage capacity, \texttt{Region} is an enumerated selectable term that declares the available regions of the service, and \texttt{costGBMonth} is a real derived term to describe the cost hour of the different configurations.

\subsubsection{HCS Terms}

As we have said before, an \hcs can define global terms that affect the whole aggregation of its configurable services
This kind of term is necessary to describe, for instance, the total cost or the discount of an \hcs, which depends on all the aggregated services. These terms are declared the terms section of the \hcs  in the same way than the rest of terms. Besides the standard operators for dependencies, order terms have available especial types -- described in Section~\ref{sec:HCSSynopsisDependencies}. 

Figure~\ref{code:StorageSynopsis} shows a couple of \hcs terms: the total cost and the discount. The value of \texttt{totalCostMonth} is calculated based on the aggregation of the \texttt{volumeCostMonth} of each storage item, while the \texttt{discount} is calculated as a 10\% of the total cost when it exceeds 3000 euros.


\subsection{Dependencies}
\label{sec:HCSSynopsisDependencies}

SYNOPSIS provides a set of expressions and operators in order to define the dependencies in the decision space. They include the classic logical, relational and arithmetic operators, and also aggregation functions to relate \hcs terms to standard terms. There are four main expression types: logical, integer, real and enumerated. Every dependency declared in SYNOPSIS should be logical, although it may be composed by other expression types. Table~\ref{tab:SYNOPSISExpressions} summarises such expressions \footnote{$\mathbb{E}$ represents the set of all enumerated values of the document.} \footnote{$sum$, $min$ and $max$ functions aggregate standard terms into global terms.}

\begin{table*}[h!!]
\centering
\caption{Dependency Expression Types for SYNOPSIS}
\label{tab:SYNOPSISExpressions}
\begin{tabular}{lp{10cm}}
%\midrule
 \textbf{Type} & \multicolumn{1}{c}{\textbf{Expressions}}\\
\midrule
 Boolean & $B ::= b \mid t_b \mid B \&\& B \mid B \parallel B \mid !B \mid B $-$> B \mid B <$-$> B \mid I > I \mid I >= I \mid I < I \mid I <= I \mid I == I \mid I != I \mid R > R \mid R >= R \mid R < R \mid R <= R \mid R == R \mid R != R \mid  E == E \mid E != E$ \\
 \midrule
 Integer & $I ::= i \mid t_i \mid I + I \mid I - I \mid I * I \mid I / I \mid -I \mid I^I \mid sum(t_i) \mid max(t_i) \mid min(t_i) $ \\
 \midrule
 Real & $R ::= r \mid t_r \mid R + R \mid R - R \mid R * R \mid R / R \mid - R \mid sum(t_r) \mid max(t_r) \mid min(t_r) $  \\
 \midrule
 Enumerated & $E ::= e \mid t_e$ \\
\end{tabular}
\begin{footnotesize}

$t_b$ any boolean term, $t_i$ any integer term, $t_r$ any real term, $t_e$ any enumerated term.

$b \in \{true,false\}, i \in \mathbb{Z}, r \in \mathbb{R}, e \in \mathbb{E}$
\end{footnotesize}
\end{table*}

Additionally, SYNOPSIS provides tables to declare groups of dependencies which involve the same terms with different values. Figure~\ref{code:StorageSynopsis} and Figure~\ref{code:ComputingSynopsis} show some examples of these tables, which are useful to describe, for instance, the characteristics of given values of a term -- as the case of computing instances -- or the pricing policies. The way the work is simple: in the first row, we declare the header of the table, i.e. the configurable terms, and the dependency relationship -- from the left to the right, split by the implication -$>$ symbol. Each additional row provides the values for each term.
