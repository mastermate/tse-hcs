% !TEX root = paper.tex
\section{Modelling Highly-Configurable Services}
\label{sec:hcs}

In order to address the need for a precise definition and comprehensive model of \hcss, we propose a \dsl that provides a complete support to describe \hcss and to perform automated analysis over those descriptions. Figure~\ref{fig:HCSAbstractModel} presents our metamodel that organises and binds all the concepts that are relevant to \hcss domain. 



\begin{figure*}[t]
\centering
\includegraphics[scale=0.80]{figures/HCSAbstractModel.pdf}
\caption{Highly-Configurable Services Metamodel.}
\label{fig:HCSAbstractModel}
\end{figure*}

Essentially, a service is described by a set of terms. In the case of a configurable service, some of these terms are decision terms\footnote{We employ configurable term and decision term as synonyms in this paper.} with at least two possible values. If the provider actually offer different alternatives for a configurable term, we say the term is selectable, or derived in other case. The different configurable terms and their dependencies conform the so-named \emph{decision space} of the service. Such decision space encompasses all the available configurations, i.e. valid combinations of configurable term values. When a configurable service allows the consumer to hire multiple and configurable items (or instances), we consider the service as a Highly-configurable Service. Finally, each of the service items can have different configurations.

As a concrete syntax to describe \hcss, we devised \emph{SYNOPSIS} (SimplY a NOtation to sPecify Service configuratIonS), which is a text-based, human-readable notation to describe the decision space of services. It supports the specification of the configuration capabilities as modelled in Fig.~\ref{fig:HCSAbstractModel}, common to services of big providers such as Amazon, Rackspace or Microsoft, while remaining provider-agnostic. Listing~\ref{code:StorageSynopsis} shows the SYNOPSIS description of a simple block storage service, which we will use as running example to explain our model and concrete notation in the following.

\begin{lstlisting}[style=synopsis,caption={Simple Block Storage Service in SYNOPSIS.},label={code:StorageSynopsis}]
Service SimpleBlockStorage {
	%SelectableTerms
		SSD: boolean;
		Size: int [1,1000];
		Region: {"USA", "EU", "JP"};
	%DerivedTerms
		costGBMonth: real [0.00,0.15]; // euros/GB per month
		volumeCostMonth: real [0.00,150]; // euros per month
	%Dependencies
		// pricing
		volumeCostMonth == costGBMonth * Size;	
		table (Region, SSD -> costGBMonth) {
			"USA", true		-> 0.1;
			"EU", true		-> 0.12;
			"JP", true		-> 0.15;
			"USA", false	-> 0.05;
			"EU", false		-> 0.06;	    
			"JP", false		-> 0.08;	    
		}
}
\end{lstlisting}

\subsection{Configurable Services}

%We consider a service as a resource or functionality delivered by a provider to one or more consumers. The service is described by a number of terms, in which the consumers can have interest. Traditional human-powered services, as electricity or public transport, present terms such as the kWh (kilowatt hour) cost for the former or the transport timetable and the ticket price for the latter. In the same way, web-based services also present terms, such as the provider, the maximum number of requests or the service price.

A configurable service in SYNOPSIS has two sections to declare terms and dependencies. The terms are categorised into two groups: selectable (\texttt{\%Se\-lec\-table\-Terms} section) and derived (\texttt{\%DerivedTerms} section). In the dependencies section (\texttt{\%Dependencies}) we can describe the relationships among the different terms.

If some of the service terms have two or more possible values, i.e. they are configurable, we say the service is \emph{configurable}, and hence its terms are \emph{decision terms}. 
For example, Amazon EBS (a block storage service) and Spotify (a music streaming service) are both configurable services.
These services can be contracted with different options, each providing specific values for the rest of decision terms. In the case of our simple storage block example, the GB cost per month depends on the region and the use of SSD. Consequently, both region and SSD are presented by the provider as direct choices, i.e. \emph{selectable terms}, while the GB cost depends on the region and SSD values chosen, so it is a \emph{derived term}.

\subsection{Decision Terms}

Decision terms are declared in SYNOPSIS in the terms sections, denoted by the \texttt{\%SelectableTerms} and \texttt{\%DerivedTerms} tags for the two disjoint categories. The main difference between them is that selectable terms determine the different configurations of the service. In the case of the service of Listing~\ref{code:StorageSynopsis}, we consider that configurations are determined by the SSD and Region terms.
However, both term types are described in the same way, using a declaration syntax close to programming languages. The only requirement is that each term should have at least two different term values. 

%As shown in Listing~\ref{code:StorageSynopsis}, we use a declaration syntax close to a programming language. First, we define the term's name, and after a colon we declare its domain type. Such domain can be enumerated (providing the values in brackets), boolean, integer (\texttt{int} in the example) or real. In case of integer or real values, we specify the domain by means of the upper and lower bounds. For real domains, we can specify the floating point as necessary in the declaration.

Listing~\ref{code:StorageSynopsis} shows the four different types of decision terms. For example, \texttt{SSD} is a boolean selectable term that indicates if the storage is ssd-based, \texttt{Size} is an integer selectable term that indicates the storage capacity, \texttt{Region} is an enumerated selectable term that declares the available regions of the service, and \texttt{costGBMonth} is a real derived term to describe the cost hour of the different configurations.

A user must choose one and only one term value for each selectable term to define a \emph{configuration}. Thus, in our example a user can only choose a region among three options: U.S.A., E.U. and Japan, while SSD offers a boolean choice and the size of the storage has to be within the specified limits. The GB cost and volume cost per month are derived terms that are not configurable but their value changes depending on the said selectable terms.

\subsection{Dependencies}
\label{sec:HCSSynopsisDependencies}

Our model defines all the values that selectable and derived terms can take. However, not any combination of them is allowed and values can be bound in different forms, affecting to the way a service can be configured. For example, cost terms depend on the size, region and ssd terms. In order to represent such dependencies, we define a set of constraints that restricts the \emph{decision space}. 

SYNOPSIS provides a set of expressions and operators in order to define the dependencies in the decision space. They include the classic logical, relational and arithmetic operators, and also aggregation functions to relate \hcs global terms to standard terms. Every dependency declared in SYNOPSIS should be logical, although it may be composed by other expression types. Table~\ref{tab:SYNOPSISExpressions} summarises such expressions.\footnote{$\mathbb{E}$ represents the set of all enumerated values of the document.}\textsuperscript{,}\footnote{$sum$, $min$ and $max$ functions aggregate standard terms into global terms.}

\begin{table*}[h!!]
\begin{center}
\begin{footnotesize}
\caption{Dependency Expression Types for SYNOPSIS}
\label{tab:SYNOPSISExpressions}
\begin{tabular}{lp{10cm}}
%\midrule
 \textbf{Type} & \multicolumn{1}{c}{\textbf{Expressions}}\\
\midrule
 Boolean & $B ::= b \mid t_b \mid B \&\& B \mid B \parallel B \mid !B \mid B $-$> B \mid B <$-$> B \mid I > I \mid I >= I \mid I < I \mid I <= I \mid I == I \mid I != I \mid R > R \mid R >= R \mid R < R \mid R <= R \mid R == R \mid R != R \mid  E == E \mid E != E$ \\
 \midrule
 Integer & $I ::= i \mid t_i \mid I + I \mid I - I \mid I * I \mid I / I \mid -I \mid I^I \mid sum(t_i) \mid max(t_i) \mid min(t_i) $ \\
 \midrule
 Real & $R ::= r \mid t_r \mid R + R \mid R - R \mid R * R \mid R / R \mid - R \mid sum(t_r) \mid max(t_r) \mid min(t_r) $  \\
 \midrule
 Enumerated & $E ::= e \mid t_e$ \\

\end{tabular}
 \end{footnotesize}
\begin{scriptsize}
$t_b$ any boolean term, $t_i$ any integer term, $t_r$ any real term, $t_e$ any enumerated term.

$b \in \{true,false\}, i \in \mathbb{Z}, r \in \mathbb{R}, e \in \mathbb{E}$
\end{scriptsize}
\end{center}
\end{table*}

Additionally, SYNOPSIS provides tables to declare groups of dependencies which involve the same terms with different values. Listing~\ref{code:StorageSynopsis} shows an example of these tables, which are useful to describe, for instance, the pricing policies. In the table constructor we declare the configurable terms and the dependency relationship (from left to right, separated by the implication -$>$ symbol). Each additional row provides the values for each term.

\subsection{Highly-Configurable Services}

The configuration capabilities of a service may go further, with the contract of multiple items, usually referred as instances, of the service that may have different configurations, or even additional linked services. This leads to the so-named Highly-Configurable Services (HCS). While some configurable services do not allow this -- e.g. Dropbox or Spotify -- others do, such as EC2 or Heroku. In the case of EC2, we can contract different computing instances of different types and in different regions, and even additional storage through the \emph{Elastic Block Storage} (EBS) service (which resembles our example in Listing~\ref{code:StorageSynopsis}), all of them related to the same Amazon account. In the case of Heroku, we can also contract different Dynos and Postgres of different types. Different items of the same service may be interrelated by means of dependencies. For instance, Amazon provides a volume-based discount which depends on the total cost of all the items contracted.

\begin{lstlisting}[style=synopsis,caption={Volume Storage HCS in SYNOPSIS.},label={code:StorageHCSSynopsis}]
Highly-configurable Service VolumeStorage{
	%Services
	storage: SimpleBlockStorage[1,*];
	
	%GlobalTerms
	totalCostMonth: real [0.00,10000.00]; 
	discount: real [0.00,1000.00];
	
	%Dependencies
	// cost aggregation
	totalCostMonth == sum(storage.volumeCostMonth);
	// discount policy
	totalCostMonth > 3000 -> 
		discount == totalCostMonth*0.1;
}
\end{lstlisting}

Listing~\ref{code:StorageHCSSynopsis} presents a \hcs in SYNOPSIS, which has three sections to declare the component services and their cardinality, global terms and global dependencies. For the two latter sections, the syntax is the same than for configurable services, as described before. In the \texttt{\%Services} section we declare the service that compose the \hcs, their cardinality (lower and upper bounds for the items), and an alias to refer to it.

Global terms affect the whole aggregation of the configurable services in a \hcs. This kind of term is necessary to describe, for instance, the total cost or the discount of an \hcs, which depends on all the aggregated services. These terms are declared in the \texttt{\%GlobalTerms} section of the \hcs. Besides the standard operators for dependencies, global terms have available especial aggregation expressions (e.g. \texttt{sum}), already enumerated in Sec.~\ref{sec:HCSSynopsisDependencies}. 

Listing~\ref{code:StorageHCSSynopsis} shows a couple of \hcs global terms: the total cost and the discount. The value of \texttt{totalCostMonth} is calculated based on the aggregation of the \texttt{volumeCostMonth} of each storage item, while the \texttt{discount} is calculated as a 10\% of the total cost when it exceeds 3000 euros.
