% !TEX root = paper.tex
\section{Introduction and Motivation}
\label{sec:intro}

A \Glsfirst{hcs} can be intuitively interpreted as a service which offers multiple configuration options, as it is defined in~\cite{lamparter2007preference}. Other authors simply employ the term ``configurable service'' in related scenarios, such as configurator systems~\cite{Heiskala2005}, configurable processes in the cloud~\cite{van2010configurable}, and configuration of real-world services~\cite{Akkermans2004}, to name a few. However, to the extent of our knowledge, there is no clear and precise definition of what a \hcs, since current definitions in the literature are rather intuitive than precise. Moreover, existing models are primarily focused on describing service characteristics, but they do not properly describe configurations. %Such definitions prevent an automatic operation and governance of this kind of services, since they introduce such a high level of variability and complexity that 

The number of actual configurations provided by a \hcs varies from just a few choices (e.g. 3 different plans in Dropbox) to thousands (16,991 possible configurations in Amazon EC2~\cite{Galan14FGCS}). Furthermore, some configurations may require the integration of additional services (e.g. Amazon EC2 requires the use of an EBS), resulting in an explosive growth in the number of configurations to be analysed. The inherent complexity of \hcss, due to the size of the decision space (i.e. the set of all possible configurations that can be chosen by users.), results in a significant added difficulty to automate related tasks, such as discovery, ranking, selection, and any kind of analysis in general.

Concerning the decision space, there are several approaches to describe it~\cite{lamparter2007preference,Heiskala2005}, though it can be defined, in general, as a subspace of the multi-dimensional space whose dimensions are given by the domain of the service terms. When the domain cardinality and dependencies among terms are numerous, the description of the decision space is an error-prone task, possibly containing anomalies and/or not capturing all possible configurations. However, service tasks should be carried out using valid descriptions and models, especially for commercial tools. Therefore, validity checking is a key analysis operation that has to be supported by the \hcss model to use for describing the decision space.
%These two major issues, i.e. the lack of precise descriptions and the resulting low level of automation, prevent the effective governance and delivery of \hcss, which becomes a key success factor when dealing with cloud computing or XaaS scenarios in general, where the amount of available services and options from different providers is increasingly high.

Regarding other related works, there are some proposals for the description and automation of \hcss, but they present drawbacks. On the one hand, some authors have proposed \fms~\cite{Kang90} to describe \hcss~\cite{wittern2012cloud,Galan14FGCS}. However, the semantic distance between \fms and \hcss, whose main concepts are features and terms respectively, is significant. Furthermore, \fms present expressiveness limitations to describe complex relationships and service compositions. On the other hand, practitioners, who are concerned about the level of automation of these services, have developed different configurators, such as CloudScreener\footnote{https://www.cloudscreener.com/}. However, these configurators hide their service specifications and often present false positives in their results~\cite{Galan14FGCS}. In this scenario, the main reasons for failing to search for optimal configurations are:
\begin{inparaenum}[(1)]
	\item the lack of sufficient facilities to model the decision space,
	\item the introduction of anomalies when instantiating the model, and
	\item the applied technique to select the best configurations.
\end{inparaenum}

In order to circumvent current limitations, we propose a \dsl that enables the rigorous definition of configurable and highly-configurable services, focusing on capturing the decision space while including interrelated concepts that play a fundamental role in relevant tasks during the service delivery workflow. Along with this \dsl, we also introduce a catalogue of operations that enable the automated analysis of certain interesting properties when specifying \hcss, hence facilitating the automated execution of service tasks enumerated before.

For validating our solution we focus on two particular tasks:
\begin{inparaenum}[(1)]
	\item validity checking for \hcss descriptions with respect to five different criteria, and
	\item selection of the best configurations with respect to user needs.
\end{inparaenum}
We interpret these common tasks in terms of well-known operations of analysis of variability and configuration models that have been already implemented and comprehensively tested~\cite{Galan14FGCS}, reusing those notions and improving the reliability of our implementation.

%For this purpose, we provide an abstract model that completely characterize \hcss, facilitating the description of their configuration capabilities and enabling automated analysis operations that can be performed in this scenario, exemplifying our solution with validity checking and selection of the best configurations.

%Regarding the actual description of \hcss, we propose a notation, namely SYNOPSIS, that allows providers and users to specify the decision space and their concrete needs and preferences, correspondingly. Based on this notation, we present a set of validity criteria for \hcss descriptions, as well as the operational support for ranking different configurations in order to select the most suitable one with respect to user requirements and preferences. Finally, we have also implemented a prototype to validate the suitability of our model and its accompanying notation for performing automated analysis.

The rest of the paper is structured as follows.
%In Sec. \ref{sec:related} we analyse existing approaches related to \hcss description and  in order to identify current limitations.
Section~\ref{sec:hcs} presents our proposed model and notation to characterise \hcss.
Then, in Sec.~\ref{sec:validity} and Sec.~\ref{sec:ranking} we discuss how to check the validity of SYNOPSIS documents and the definition of user needs, correspondingly.
We present our automated solution to perform various analysis operations in Sec.~\ref{sec:analysis}. Based on that solution, we present our prototype implementation in Sec.~\ref{sec:validation}. Finally, Sec.~\ref{sec:conclusions} concludes the paper and provide some insights on future work.